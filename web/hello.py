from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/test')
def index2():
    return '<h1>Hello World</h1>'

# 避免使用到重複函數
# index index2

@app.route('/user/<name>')
def user(name):
    return '<h1>Hello World %s !!</h1>' %name

if __name__ == '__main__':
    app.run(debug=True)